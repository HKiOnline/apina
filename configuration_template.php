<?php

/**
*  APNS Service related configuration
*/
define("APINA_APNS_DEVELOPMENT_GATEWAY_URL", "tls://gateway.sandbox.push.apple.com:2195");
define("APINA_APNS_DEVELOPMENT_FEEDBACK_URL", "tls://feedback.sandbox.push.apple.com:2196");
define("APINA_APNS_DEVELOPMENT_CERTIFICATE_FILE_PATH", "/path/to/dev-combined.pem");
define("APINA_APNS_DEVELOPMENT_PASSWORD", "PutYourDevPasswordHere");

define("APINA_APNS_PRODUCTION_GATEWAY_URL", "tls://gateway.push.apple.com:2195");
define("APINA_APNS_PRODUCTION_FEEDBACK_URL", "tls://feedback.push.apple.com:2196");
define("APINA_APNS_PRODUCTION_CERTIFICATE_FILE_PATH", "/path/to/prod-combined.pem");
define("APINA_APNS_PRODUCTION_PASSWORD", "PutYourProdPasswordHere");

define("APINA_APNS_CACERT_FILE_PATH", "/path/to/entrust_2048_ca.pem");

/**
* App specific configuration
*/

define("APINA_SQLITE_DB_PATH", "/path/to/apns-notifications.db");

?>
