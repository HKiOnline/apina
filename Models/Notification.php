<?php

/**
* @author: Henri Kesseli
*/
class Notification extends DB{

	public $id;
	public $user;
	public $title;
	public $message;
	public $token;


	function get($id){
		return $this->run("SELECT * FROM Notifications WHERE id = :notificationId", ":notificationId", "$notificationId");
	}

	function getByUserId($userId){
		return $this->run("SELECT * FROM Notifications WHERE user=:userId", ":userId", "$userId");
	}

	function getUnsent(){
		return $this->run("SELECT * FROM Notifications WHERE state='unsent'");
	}

	function getSent(){
		return $this->run("SELECT * FROM Notifications WHERE state='sent'");
	}


	function store($sent = false){

		$state = "unsent";
		if($sent){
			$state = "sent";
		}

		$statement = $this->prepare("INSERT INTO Notifications (title, message, user, state) VALUES (:title, :message, :userId, :state)");
		$statement->bindValue(":userId", $this->user);
		$statement->bindValue(":title", $this->title);
		$statement->bindValue(":message", $this->message);
		$statement->bindValue(":state", $state);
		$result = $statement->execute();


		$statement = $this->prepare("SELECT * FROM Notifications WHERE title=:title AND message=:message AND user=:userId AND state=:state ORDER BY created DESC LIMIT 1");
		$statement->bindValue(":userId", $this->user);
		$statement->bindValue(":title", $this->title);
		$statement->bindValue(":message", $this->message);
		$statement->bindValue(":state", $state);
		$result = $statement->execute();

		$stored_notification = $result->fetchArray(SQLITE3_ASSOC);
		$this->id = $stored_notification["id"];


		$statement->close();

	}

		function changeStateAsSent(){
			$this->run("UPDATE Notifications SET state='sent' WHERE id=:id", ":id", $this->id);
		}
}

?>
