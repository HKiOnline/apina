<?php

/**
* @author: Henri Kesseli
*/
class DB extends SQLite3{


	function __construct(){
		$db_path = APINA_SQLITE_DB_PATH;
		$this->open($db_path);
	}

	protected function run($query, $bindId=null, $bindValue=null){

		$statement = $this->prepare($query);

		if(!is_null($bindId) && !is_null($bindValue)){
			$statement->bindValue($bindId, $bindValue);
		}

		$results = $statement->execute();
		$resultsArray = array();

		while ($result = $results->fetchArray(SQLITE3_ASSOC)) {
			$resultsArray[] = $result;
		}

		$statement->close();
		return $resultsArray;
	}
}

?>
