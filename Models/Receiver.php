<?php

/**
* @author: Henri Kesseli
*/
class Receiver extends DB {

	public $id;
	public $devicename;
	public $token;


	function __construct($id=null, $is_devicename=true){

		parent::__construct();

		if(!is_null($id) && $is_devicename){
			$this->getByDevicename($id);
		}elseif(!is_null($id) && !$is_devicename){
			$this->get($id);
		}
	}

	function get($id){
		$user = $this->run("SELECT * FROM Receivers WHERE id=:id", ":id", $id);
		$user = $user[0];

		$this->id = $user["id"];
		$this->username = $user["devicename"];
		$this->token = $user["token"];

		return $user;


	}
	function getByDevicename($devicename){
		$user = $this->run("SELECT * FROM Receivers WHERE devicename=:devicename", ":devicename", $devicename);
		$user = $user[0];

		$this->id = $user["id"];
		$this->username = $user["devicename"];
		$this->token = $user["token"];

		return $user;
	}

	function getByAPNsToken($token){
		$user = $this->run("SELECT * FROM Receivers WHERE token=:token", ":token", $token);
		$user = $user[0];

		$this->id = $user["id"];
		$this->username = $user["devicename"];
		$this->token = $user["token"];

		return $user;
	}

	function getAll(){

		$users = $this->run("SELECT * FROM Receivers");
		return $users;
	}

	function getAllActive(){

		$users = $this->run("SELECT * FROM Receivers WHERE active='y'");
		return $users;
	}

	function store(){

		$activity_state = null;
		$statement = null;

		if($this->active){
			$activity_state = "y";
		}else{
			$activity_state = "n";
		}

		if(isset($this->id)){
			$statement = $this->prepare("UPDATE Receivers SET devicename=:devicename, token=:token, active=:active WHERE id=:id");
			$statement->bindValue(":id", $this->id);
		}else{
			$statement = $this->prepare("INSERT INTO Receivers (devicename, token, active) VALUES (:devicename, :token, :active)");
		}

		$statement->bindValue(":devicename", $this->devicename);
		$statement->bindValue(":token", $this->token);
		$statement->bindValue(":active", $activity_state);
		$result = $statement->execute();
	}

}

?>
