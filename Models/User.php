<?php

/**
* @author: Henri Kesseli
*/
class User extends DB {

	public $username;
	public $salt;
	public $password;


	function __construct(){
		parent::__construct();
	}

	function authenticate($username, $password){

		// Get password and salt with username
		// Add inputted password to salt and compare to stored password

		$user = $this->run("SELECT * FROM Users WHERE username=:username", ":username", $username);
		$user = $user[0];

		$stored_username = $user["username"];
		$stored_password = $user["password"];
		$stored_salt = $user["salt"];

		if( ($stored_username == $username) && (hash("sha256", $stored_salt.$password) == $stored_password) ){
			return true;
		}else{
			return false;
		}

	}

}

?>
