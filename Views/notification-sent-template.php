<h1>Notification was sent</h1>
<p>Notification was sent with best effort.</p>
<ul>
	<li>Title: <?php echo $title ?></li>
	<li>Message: <?php echo $message ?></li>
	<li>Receiver: <?php echo $receiver ?></li>
</ul>
<a href="/" title="Back to frontpage"><< Back to frontpage</a>