<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Apina :: APNS Client</title>
  <meta name="description" content="What Would Gandalf Say, Gandalf quotes">
  <meta name="author" content="HKi">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
    .vertical-container {
      display: -webkit-flex;
      display: flex;
      -webkit-align-items: center;
      align-items: center;
      -webkit-justify-content: center;
      justify-content: center;
    }

    #quote {
      max-width:70%;
    }
  </style>
</head>

<body>