<?php
	spl_autoload_register();

	$basepath = getcwd();

	// Require statements
	require($basepath."/configuration_local.php");
	require($basepath."/Router.php");

	// Data model
	require($basepath."/Models/DB.php");
	require($basepath."/Models/User.php");
	require($basepath."/Models/Receiver.php");
	require($basepath."/Models/Notification.php");

	// Controllers
	require($basepath."/Controllers/APNslib.php");
	require($basepath."/Controllers/APNsProvider.php");
	require($basepath."/Controllers/APNsRegistrator.php");

?>
