# Apina - Apple Push Notification Service Provider

Apina is a simple APNS provider. It is built mainly for testing purposes and should be considered "beta". That means that it is not polished/complete I would like it to be.

## Features
- Simple HTTP interface to register and unregister iOS-devices for notification receivers
- Send notifications to registered users via web UI

## Dependencies and requirements
- PHP 5.3 or higher
- PHP sqlite3 support
- Apple developer account (if you actually wish to send notifications)
- Credentials pem-file

## Installation

Steps:

  1. sqlite db intialization
  2. starting php interpreter with all requests directed to App.php
  3. there is no step 3


First you need to create sqlite database under the Models-directory. To do this run:

    sqlite3 Models/apns-notifications.db < Models/apns-notifications-db-schema.sql

Note the directory path. The example assumes you are in the root of the project directory.

## Security and using Apina in production

_incomplete instructions_

Apina is made mainly for testing. My aim is to make the project production suitable, but it is not at that state yet. So __do not use this project in production yet.__ The API should not be exposed with out additional security measures. Take good care of your developer pem-file!


## Using only the APNslib

_incomplete instructions_

This project includes the complete stack for APNS integration testing for your iOS app. However, the most important bits are in single file. Namely, in the APNslib-file. This file includes the APNsNotification -class, the APNsClient -class and the APNsFeedbackClient-class.

The APNsNotification manages the notification format and servers as data model or a template for the notification. APNsNotification is constructed and then passed to APNsClient that handles communication with Apple's APNS gateway. To get an idea how these are used, have a look at the APNsProvider-class under the Controllers-directory.

## License
License name: _The BSD License (OSI), Modified BSD License (FSF)_

Copyright (c) 2015, Henri Kesseli
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

- Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
- Neither the name of the Henri Kesseli or HKiOnline nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL HENRI KESSELI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## TODO
- instructions how to make a pem-file
- missing router method implementation (should be its own project, it's handy)
