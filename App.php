<?php
require_once 'autoloader.php';

$router = new Router();
session_start();

$router->get("/", function() {

	if($_SESSION["loggedIn"] == true){

		// Store and send notifications to users
		$registrator = new APNsRegistrator();
		$registeredReceivers = $registrator->getRegisteredReceivers();

		require 'Views/header-template.php';
		require 'Views/index-body-template.php';
		require 'Views/send-notification-form-template.php';
		require 'Views/footer-template.php';

	}else{
		header("Location: /login");
		exit();
	}


});

$router->post("/", function($params) {

	if($_SESSION["loggedIn"] == true){

		syslog(LOG_INFO, "Sending a notification to APNS");

		// Get notification params
		$title = $params["title"];
		$message = $params["message"];
		$receiver = $params["users"];
		$environment = $params["environment"];

		$provider = new APNsProvider();
		$provider->sendNotification($receiver, $title, $message, $environment);

		require 'Views/header-template.php';
		require 'Views/notification-sent-template.php';
		require 'Views/footer-template.php';

	} else {
		header("Location: /login");
		exit();
	}
});

$router->get("/login", function() {
	require 'Views/header-template.php';
	require 'Views/login-form-template.php';
	require 'Views/footer-template.php';
});

$router->post("/login", function($params) {

	// Get login params
	$username = $params["username"];
	$password = $params["password"];

	$user = new User();
	if($user->authenticate($username, $password)){

		$_SESSION["loggedIn"] = true;
		header("Location: /");
		exit();
	}else{
		require 'Views/header-template.php';
		require 'Views/login-form-template.php';
		require 'Views/footer-template.php';
	}

});

$router->get("/logout", function() {
	$_SESSION["loggedIn"] = false;
	session_destroy();
	header("Location: /login");

});


$router->get("/registrar", function ($params){

	// Register and unregister users for notification

	// This returns JSON, not HTML
	header('Content-Type: application/json');
	$username = null;

	if( isset($params["username"]) ){
		$username = $params["username"];
	}elseif( !is_null($_SERVER["HTTP_X_APINA_USERNAME"]) ){
		$username = $_SERVER["HTTP_X_APINA_USERNAME"];
	}else{
		echo '{"Status":"Error", "message":"Username missing."}';
		exit();
	}


	if( isset($params["start"]) ){

		$token = $params["start"];

		$registrator = new APNsRegistrator();
		$registeredUser = $registrator->register($username, $token);

		echo '{"Status":"Success", "message":"Device registered to receive notifications."}';
		syslog(LOG_INFO, "Successfully registered a device.");

	}elseif(isset($params["stop"])){

		$token = $params["stop"];

		$registrator = new APNsRegistrator();
		$registeredUser = $registrator->unregister($username);

		echo '{"Status":"Success", "message":"Device unregistered."}';
		syslog(LOG_INFO, "Successfully unregistered a device.");

	}else{
		echo '{"Status":"Error", "message":"Token missing."}';
		syslog(LOG_INFO, "Failed to register a device. Token missing.");
	}


	exit();
});

$router->get("/receivers/expired", function() {

	if($_SESSION["loggedIn"] == true){
		// Check if any users token have expired (because they have uninstalle the app)
		$registrator = new APNsRegistrator();
		$expired_tokens = $registrator->checkExpiredTokens();
		$expired_tokens_count = $registrator->count;

		syslog(LOG_INFO, "Querying APNS Feedback Service. $expired_tokens_count expired tokens.");

		require 'Views/header-template.php';
		require 'Views/expired-users-body-template.php';
		require 'Views/footer-template.php';
	}else{
		header("Location: /login");
		exit();
	}
});

$router->fallback(function (){
	require 'Views/header-template.php';
	echo "<h1>404: Unknown route!</h1>";
	require 'Views/footer-template.php';
});


?>
