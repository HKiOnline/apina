<?php

class Router {

	private $serverpath;
	private $method;
	private $controllerPath;
	private $fallback;

	function __construct($fallback = true){
		$this->serverpath = $_SERVER['REQUEST_URI'];

		if(isset($_SERVER['QUERY_STRING'])){
			$this->params_string = $_SERVER['QUERY_STRING'];
		}else{
			$this->params_string = null;
		}


		$this->method = $_SERVER['REQUEST_METHOD'];
		$this->controllerPath = "/controllers";
		$this->fallback = $fallback;
 	}

 	function getRequestURI(){
 		return $this->serverpath;
 	}

 	function getMethod(){
 		return $this->method;
 	}

 	function isGet(){
 		if($this->getMethod() == "GET"){
 			return TRUE;
 		}else{
 			return FALSE;
 		}
 	}

 	function isPost(){
 		if($this->getMethod() == "POST"){
 			return TRUE;
 		}else{
 			return FALSE;
 		}
 	}

 	function isPut(){
 		if($this->getMethod() == "PUT"){
 			return TRUE;
 		}else{
 			return FALSE;
 		}
 	}

 	function get($path, $callback){
 		if($this->isGet()){

 			$serverpath = $this->serverpath;

 			if(!is_null($this->params_string)){
 				$serverpath = str_replace("?".$this->params_string, "", $this->serverpath);
 			}

 			if($serverpath == $path){
 				$this->fallback = false;
 				call_user_func($callback, $_GET);
 			}
 		}
 	}

 	function post($path, $callback){
 		if($this->isPost()){

 			if($this->serverpath == $path){
 				$this->fallback = false;
 				call_user_func($callback, $_POST);
 			}
 		}
 	}

 	function fallback($callback){
 		if($this->fallback){
 			call_user_func($callback);
 		}
 	}

}

?>
