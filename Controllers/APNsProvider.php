<?php

/**
* @author: Henri Kesseli
*/
class APNsProvider{

	function __constructor(){

	}

	function sendNotification($receiver, $title, $message, $environment){

		// Get user information
		// Receiver is a unique username
		$user = new Receiver($receiver);

		// Create new notification
		$notification = new Notification();

		$notification->user = $user->id;
		$notification->title = $title;
		$notification->message = $message;
		$notification->token = $user->token;

		// Store the notification
		$notification->store();

		// APNsNotification is a notification format template
		$apns_notification = new APNsNotification();
		$apns_notification->setSimpleAlert($notification->message);

		if($environment == "production"){
			$gateway_url = APINA_APNS_PRODUCTION_GATEWAY_URL;
			$cert_file_path = APINA_APNS_PRODUCTION_CERTIFICATE_FILE_PATH;
			$password = APINA_APNS_PRODUCTION_PASSWORD;
			$cacert_filepath = APINA_APNS_CACERT_FILE_PATH;
		}elseif ($environment == "development") {
			$gateway_url = APINA_APNS_DEVELOPMENT_GATEWAY_URL;
			$cert_file_path = APINA_APNS_DEVELOPMENT_CERTIFICATE_FILE_PATH;
			$password = APINA_APNS_DEVELOPMENT_PASSWORD;
			$cacert_filepath = APINA_APNS_CACERT_FILE_PATH;

		}else{
			trigger_error("Provider missconfiguration. Given environment '$environment' is not acceptable. Environment must be production or development.",E_USER_ERROR);
			exit();
		}


		// Send the notification
		$apns_client = new APNsClient(
			$gateway_url,
			$cert_file_path,
			$password,
			$cacert_filepath
		);
		$success = $apns_client->send($apns_notification->getPayload(), $notification->token);

		if($success){
			// Set notification state as sent
			$notification->changeStateAsSent();
		}

	}

}

?>
