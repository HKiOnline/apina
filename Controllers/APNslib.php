<?php

/**
* @author: Henri Kesseli
*
* Payload structure docs: https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Chapters/ApplePushService.html
* Notification binary interface docs: https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Chapters/CommunicatingWIthAPS.html#//apple_ref/doc/uid/TP40008194-CH101-SW4
*/
class APNsNotification{

	private $token;
	private $alert;
	private $badge;
	private $sound;
	private $content_available;

	function __construct(){

		// set default values
		$this->alert = "You have a new notification!"; // Simple message
		$this->badge = 0;															 // 0 removes badges, above 0 means badge count
		$this->sound = "default";
		$this->content_available = false;

	}

	private function buildPayload(){

		$notification = array();
		$aps = array();

		$aps["alert"] = $this->getAlert();
		$aps["badge"] = $this->getBadge();
		$aps["sound"] = $this->getSound();

		if($this->getContentAvailable()){
			$aps["content-available"] = 1;
		}

		$notification["aps"] = $aps;

		// Not yet supported
		// $notification[$custom_property_namespace] = array();

		return json_encode($notification);
	}

	public function setSimpleAlert($message){
		$this->alert = $message;
	}
	public function getAlert(){
		return $this->alert;
	}

	public function setBadge($count){

		if(is_int($count)){
				$this->badge = $count;
		}

	}
	public function getBadge(){
		return $this->badge;
	}

	public function setSound($sound){
		$this->sound = $sound;
	}
	public function getSound(){
		return $this->sound;
	}

	public function setContentAvailable($content_availabke){

		if(is_bool($content_availabke)){
				$this->content_available = $content_availabke;
		}
	}

	public function getContentAvailable(){
		return $this->content_available;
	}

	public function setToken($token){
		$this->token = $token;
	}
	public function getToken(){
		return $this->token;
	}

	public function getPayload(){
		return $this->buildPayload();
	}

}


/**
* @author: Henri Kesseli
* APNS Push Notification Client,
* This class sends the notification messages to APNs that handles the notificatio delivery
* to devices that have registered to APNs to receive messages
*/
	class APNsClient{

		private $gatewayUri;
		private $connected;
		private $apns_passphrase;
		private $apns_certificate;
		private $apns_ca;
		private $fp;

		function __construct($gateway, $certificate, $passphrase, $cacerts){

			$this->gatewayUri = $gateway;
			# APNS certificate is tied to a developer, you need to get your own via Apple Dev Portal
			$this->apns_certificate = $certificate;
			# Certificate passphrase, not terribly sensitive (requires the pem) but change this still
			$this->apns_passphrase = $passphrase;
			$this->apns_ca = $cacerts;
			$this->connected = false;
		}

		/**
		* Send a single push notification
		* Please use the APNsNoticifation class to build the payload
		*/
		function send($payload, $token){

			$this->stream_builder();

			if($this->connected){
				$result = $this->notification_writer($payload, $token);
				fclose($this->fp);
				return result;
			}else{
				trigger_error("Cannot send notification. Not connected to APNS.",E_USER_ERROR);
				return false;
			}
		}

		/**
		* Send several push notifications at once
		* Must be array of APNsNoticifation objects!
		*/
		function sendNotifications($notifications){

			if(is_array($notifications)){
				$this->stream_builder();

				if($this->connected){

					foreach ($notifications as $notification) {

						if(is_a($notification, "APNsNotification")){
							$result = $this->notification_writer($notification->getPayload(), $notification->getToken());
						}else{
							trigger_error("Not a APNsNotification type object. Notifications must be an array of notifications.",E_USER_ERROR);
						}
					}
					fclose($this->fp);
					return $result;
				}else{
					trigger_error("Cannot send notifications. Not connected to APNS.",E_USER_ERROR);
					return false;
				}
			}else{
				trigger_error("Not an array. Notifications must be an array of notifications.",E_USER_ERROR);
				return false;
			}
		}

		/**
		* Connects to APNS servers by building a TCP stream,
		* remember to close the stream!
		*/
		private function stream_builder(){


			if (!file_exists($this->apns_certificate)) {
					trigger_error("Certificate file not found. Stopped building stream connection.",E_USER_ERROR);
					exit();
			}

			// Create and setup stream
			$ctx = stream_context_create();
			stream_context_set_option($ctx, "ssl", "cafile", $this->apns_ca);
			stream_context_set_option($ctx, "ssl", "local_cert", $this->apns_certificate);
			stream_context_set_option($ctx, "ssl", "passphrase", $this->apns_passphrase);


			// Connect to Apple APNs Server
			$this->fp = stream_socket_client($this->gatewayUri, $error, $error_message, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

			// If we fail to connect we may end execution now
			if(!$this->fp){
				trigger_error("Failed to connect to APNS: ".$error." ".$error_message,E_USER_ERROR);
			}else{
				// Set state to connected
				$this->connected = true;
			}
		}

		/**
		* Writes notifcations to the already opened stream
		*/
		private function notification_writer($payload, $token){

			if($this->connected){

				// Build notification
				$frame_length = 0;

				// token:  item id is 1, then we have size (2 bytes) and finally the payload
				$token_item_length = strlen(pack("H*", $token));
				$token_item = chr(1).pack("n", $token_item_length).pack("H*", $token); // length should be 23 bytes
				$frame_length += strlen($token_item);

				// payload: item id is 2, then we should have size (32 bytes) and finally the payload
				$payload_item = chr(2).pack("n", strlen($payload)).$payload;
				$frame_length += strlen($payload_item);

				// notification identifier: item id is 3, then we have size (4 bytes) and finally the identifier
				$notification_id = rand();
				$notification_id_length = strlen(pack("N", $notification_id));
				$notification_id_item = chr(3).pack("n", $notification_id_length).pack("N", $notification_id);
				$frame_length += strlen($notification_id_item);

				// expiry: item id is 4, then we have size (4 bytes) and finally the expiration time as unix time
				// note 0 means that message is not stored if it fails
				$expiry = 0;
				$expiry_item = chr(4).pack("n", 4).pack("N", 0);
				$frame_length += strlen($expiry_item);

				// priority: item id is 5, then we have size (1 bytes) and then priority 5 or 10
				// 10 is now, 5 is energy conservative
				$priority = chr(10);
				$priority_item = chr(5).pack("n", strlen($priority)).$priority;
				$frame_length += strlen($priority_item);

				$prefix = chr(2).pack("N", $frame_length);
				$notification = $prefix.$token_item.$payload_item.$notification_id_item.$expiry_item.$priority_item;

				// Write and return true/false based on success
				return fwrite($this->fp, $notification, strlen($notification));
			}else{
				trigger_error("Cannot write to APNS stream. Not connected.",E_USER_ERROR);
				return false;
			}
		}
	}


	/**
	* APNS Feedback Service Query Client
	* Push notification providers must query APNS for devices that have unistalled the app that is was used to register
	* with APNs. Apple considers it to bad practice to send messages to devices that are unable to receive them.
	*/
	class APNsFeedbackClient{

		private $gatewayUri;
		private $connected;
		private $apns_passphrase;
		private $apns_certificate;
		private $apns_ca;

		function __construct($gateway, $certificate, $passphrase, $cacerts){
			$this->apns_passphrase = $passphrase;
			$this->apns_certificate = $certificate;
			$this->apns_ca = $cacerts;
			$this->gatewayUri = $gateway;
		}

		function getExpiredTokens(){

			// Setup and open stream to APNS feedback service
			$ctx = stream_context_create();
			stream_context_set_option($ctx, "ssl", "cafile", $this->apns_ca);
			stream_context_set_option($ctx, "ssl", "local_cert", $this->apns_certificate);
			stream_context_set_option($ctx, "ssl", "passphrase", $this->apns_passphrase);

			// Connect to APNS Feedback Service
			$apns = stream_socket_client($this->gatewayUri, $errcode, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
			if(!$apns) {
					trigger_error("Could not connect to APNS Feedback Service. Failed to create a stream ($errcode: $errstr)",E_USER_ERROR);
					return;
			}


			$expired_tokens = array();
			//and read the data on the connection:
			while(!feof($apns)) {
					$data = fread($apns, 38);
					if(strlen($data)) {
						// time stamp (4 bytes) + token length (2 bytes) + device token (32 bytes)
						$expired_tokens[] = unpack("N1timestamp/n1length/H*devtoken", $data);
					}
			}
			fclose($apns);
			return $expired_tokens;

		}

	}


?>
