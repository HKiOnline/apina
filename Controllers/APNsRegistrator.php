<?php

/**
* @author: Henri Kesseli
*/
class APNsRegistrator{
	public $count = 0;


	function register($receiver_devicename, $token){

		$receiver = new Receiver($receiver_devicename);

		if(is_null($receiver->id)){
			$receiver = new Receiver();
		}

		$receiver->devicename = $receiver_devicename;
		$receiver->token = $token;
		$receiver->active = true;
		$receiver->store();

	}

	function unregister($receiver_devicename){

		$receiver = new Receiver($receiver_devicename);
		$receiver->active = false;
		$receiver->store();

	}

	function getRegisteredReceivers(){

		$receivers = new Receiver();
		return $receivers->getAllActive();

	}

	function checkExpiredTokens(){
		// $certificate, $passphrase, $cacerts
		$feedback_service = new APNsFeedbackClient(
			APINA_APNS_FEEDBACK_URL,
			APINA_APNS_DEVELOPER_CERTIFICATE_FILE_PATH,
			APINA_APNS_DEVELOPER_PASSWORD,
			APINA_APNS_CACERT_FILE_PATH
		);
		$expired_tokens = $feedback_service->getExpiredTokens();
		$this->count = count($expired_tokens);


		if($this->count > 0){
				return $expired_tokens;
		}else{
				return null;
		}
	}

}


?>
